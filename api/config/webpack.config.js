var webpack = require('webpack')
var _debug = require('debug')
var path = require('path')
var nodeExternals = require('webpack-node-externals');
var env = require('../env')

const debug = _debug('app:webpack:config')
const paths = env.utils_paths
const {__DEV__, __INT__, __STG__, __PROD__} = env.globals

debug('Webpack Configuration')

config = {
  entry: './app/bin/www.js',
  target: 'node',
  resolve: {
    modules: [
      paths.base(env.dir_client),
      "node_modules"
    ]
  },
  externals: [nodeExternals()],
  devtool: 'sourcemap',
  output: {
    path: paths.base(env.dir_dist),
    filename: 'api.js'
  },
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      enforce: 'pre',
      loader: 'eslint-loader',
      exclude: /node_modules/
    },
    {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        cacheDirectory: true,
        plugins: ['transform-object-rest-spread'],
        presets: ['es2015'],
        env: {
          development: {
            retainLines: true
          }
        }
      }
    }
  ],
  },
  plugins: [
    new webpack.BannerPlugin({banner: 'require("source-map-support").install();', raw: true, entryOnly: false }),
    new webpack.LoaderOptionsPlugin({
      options: {
       eslint: {
         configFile: path.join(__dirname, '.eslintrc'),
         emitWarning: __DEV__
       }
      }
    })
  ]
}

if (__DEV__) {
  debug('Enable plugins for live development (Define, and NoEmitOnErrors).')
  config.plugins.push(
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEV__: __DEV__,
      __INT__: __INT__,
      __STG__: __STG__,
      __PROD__: __PROD__,
      API_VERSION: JSON.stringify("v" + require("../package.json").version),
      UI_VERSION: JSON.stringify("v" + require("../../ui/package.json").version)
    })
  )
}

if (__PROD__) {
  debug('Enable plugins for production (Define, OccurenceOrder, and UglifyJS).')
  config.plugins.push(
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        unused: true,
        dead_code: true,
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      __DEV__: __DEV__,
      __INT__: __INT__,
      __STG__: __STG__,
      __PROD__: __PROD__,
      API_VERSION: JSON.stringify("v" + require("../package.json").version),
      UI_VERSION: JSON.stringify("v" + require("../../ui/package.json").version)
    })
  )
}

module.exports = config
