import app from '../app'
import store from '../redux/store'
import { createServer } from 'http'
import { error, info } from '../utils/logger'
import { connect } from '../db/db'
import { findByLastAuthenticated } from '../db/users'
import { sessionActions } from '../redux/sessions'
import { updateSession } from '../ws/handler'
import { startWss } from '../ws/wss'

const createSession = (username) => {
  return new Promise((resolve, reject) => {
    const { createSession } = sessionActions
    store.dispatch(createSession(username))
    updateSession(username, true).then(() => resolve()).catch(err => reject(err))
  })
}

connect().then(() => {
  info('Connected to mongo')

  findByLastAuthenticated().then(docs => {
    const sessionPromises = docs.map(doc => createSession(doc.username))
    Promise.all(sessionPromises).catch(err => error(err))
  }).catch(err => error(err))

  // Setup port
  const port = (val => {
    const port = parseInt(val, 10)

    if (isNaN(port)) {
      // named pipe
      return val
    }

    if (port >= 0) {
      // port number
      return port
    }

    return false
  })(process.env.PORT || '3000')
  app.set('port', port)

  const server = createServer(app)
  startWss(server)

  server.listen(port)

  server.on('error', error => {
    if (error.syscall !== 'listen') {
      throw error
    }

    const bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        error('%s requires elevated privileges', bind)
        process.exit(1)
      case 'EADDRINUSE':
        error('%s is already in use', bind)
        process.exit(1)
      default:
        throw error
    }
  })

  server.on('listening', () => {
    const addr = server.address()
    const bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port
    info('Listening on %s', bind)
  })
}).catch(err => {
  error('Unable to connect to MongoDB! %j', err)
  process.exit(1)
})
