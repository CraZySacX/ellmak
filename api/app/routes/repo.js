import express from 'express'
import { findIdByUsername } from '../db/users'
import { findByUserId, removeByShortName, upsertByShortName } from '../db/repos'
import { deleteRepoDir, openOrClone } from '../git/repo'
import { error } from '../utils/logger'

const router = express.Router()

router.get('/', (req, res, next) => {
  const { username } = req.query

  findIdByUsername(username).then((id) => {
    findByUserId(id).then((docs) => {
      return res.status(200).send(docs)
    }).catch((err) => {
      error(err)
      return res.status(500).send('Unable to lookup repository information for user')
    })
  }).catch((err) => {
    error('Unable to determine user:', err)
    return res.status(500).send('Unable to determine user')
  })
})

router.post('/', (req, res, next) => {
  const { username } = req.body

  findIdByUsername(username).then((id) => {
    upsertByShortName(id, req.body).then((doc) => {
      openOrClone(doc).catch(err => error('Unable to clone repo:', err))
      return res.status(200).send(doc)
    }).catch((err) => {
      error(err)
      return res.status(500).send('Unable to upsert repository information')
    })
  }).catch((err) => {
    error(err)
    return res.status(500).send('Unable to determine user')
  })
})

router.delete('/', (req, res, next) => {
  const { shortName } = req.body

  removeByShortName(shortName).then(doc => {
    if (doc.result.n === 1) {
      deleteRepoDir(shortName).then(() => {
        return res.status(200).send(doc)
      }).catch(err => {
        error('Unable to delete repository directory for' + shortName + ': ' + err)
        return res.status(500).send('Unable to delete repository directory')
      })
    } else {
      error('Delete count was not 1: ' + doc.result.n)
      return res.status(500).send('Delete count was not 1')
    }
  }).catch(err => {
    error('Unable to delete repository ' + shortName + ': ' + err)
    return res.status(500).send('Unable to delete repository')
  })
})

export default router
