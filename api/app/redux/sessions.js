import _ from 'lodash'

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------
export const ELLMAK_SESSION_CREATE = 'ellmak/session/create'
export const ELLMAK_SESSION_ADD_USERID = 'ellmak/session/add/userid'
export const ELLMAK_SESSION_ADD_WEBSOCKET = 'ellmak/session/add/websocket'
export const ELLMAK_SESSION_REMOVE_WEBSOCKET = 'ellmak/session/remove/websocket'

// ----------------------------------------------------------------------
// Actions
// ----------------------------------------------------------------------
const createSession = (userName) => {
  return {
    type: ELLMAK_SESSION_CREATE,
    userName: userName
  }
}

const addUserId = (userName, userId) => {
  return {
    type: ELLMAK_SESSION_ADD_USERID,
    userName: userName,
    userId: userId
  }
}

const addWebSocket = (userName, wsKey, ws) => {
  return {
    type: ELLMAK_SESSION_ADD_WEBSOCKET,
    userName: userName,
    wsKey: wsKey,
    ws: ws
  }
}

const removeWebSocket = (userName, wsKey) => {
  return {
    type: ELLMAK_SESSION_REMOVE_WEBSOCKET,
    userName: userName,
    wsKey: wsKey
  }
}

// ----------------------------------------------------------------------
// Exported Actions, useful for use with bindActionCreators
// ----------------------------------------------------------------------
const sessionActions = {
  addUserId,
  addWebSocket,
  createSession,
  removeWebSocket
}

// ----------------------------------------------------------------------
// Action Handlers
// ----------------------------------------------------------------------
const ACTION_HANDLERS = {
  [ELLMAK_SESSION_CREATE]: (state, action) => {
    return {
      ...state,
      [action.userName]: {
        userId: '',
        webSockets: {}
      }
    }
  },
  [ELLMAK_SESSION_ADD_USERID]: (state, action) => {
    return { ...state, [action.userName]: { ...state[action.userName], userId: action.userId } }
  },
  [ELLMAK_SESSION_ADD_WEBSOCKET]: (state, action) => {
    return {
      ...state,
      [action.userName]: {
        ...state[action.userName],
        webSockets: {
          ...state[action.userName].webSockets,
          [action.wsKey]: action.ws
        }
      }
    }
  },
  [ELLMAK_SESSION_REMOVE_WEBSOCKET]: (state, action) => {
    return {
      ...state,
      [action.userName]: {
        ...state[action.userName],
        webSockets: _.omit(state[action.userName].webSockets, action.wsKey)
      }
    }
  }
}

// ----------------------------------------------------------------------
// Initial Database State
// ----------------------------------------------------------------------
const initialState = {}

// ----------------------------------------------------------------------
// Reducer
// ----------------------------------------------------------------------
const reducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

export { reducer as default, sessionActions }
