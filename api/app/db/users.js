import _ from 'lodash'
import moment from 'moment'
import store from '../redux/store'
import { error, warn } from '../utils/logger'

const findByUsername = (username) => {
  return new Promise((resolve, reject) => {
    const db = store.getState().db

    if (db && !_.isEmpty(db.conn)) {
      const conn = db.conn
      const usersCollection = conn.collection('users')

      usersCollection.findOne({'username': username}, (err, doc) => {
        if (err) {
          error('User %s not found: %j', username, err)
          reject(new Error('User not found'))
        } else if (!_.isEmpty(doc)) {
          resolve(doc)
        } else {
          warn('User %s not found', username)
          reject(new Error('No user documents found'))
        }
      })
    } else {
      warn('Database not valid')
      warn('User %s not found', username)
      reject(new Error('User not found'))
    }
  })
}

const findByObjWithUsername = (obj) => {
  const { username } = obj
  return new Promise((resolve, reject) => {
    findByUsername(username).then((userDoc) => {
      obj.user = userDoc
      resolve(obj)
    }).catch(err => reject(err))
  })
}

const findIdByUsername = (username) => {
  return new Promise((resolve, reject) => {
    const db = store.getState().db

    if (db && !_.isEmpty(db.conn)) {
      const conn = db.conn
      const usersCollection = conn.collection('users')

      usersCollection.findOne({'username': username}, (err, doc) => {
        if (err) {
          error('User %s not found: %j', username, err)
          reject(new Error('User not found'))
        } else if (!_.isEmpty(doc)) {
          resolve(doc._id)
        } else {
          warn('User %s not found', username)
          reject(new Error('No user documents found'))
        }
      })
    } else {
      warn('Database not valid')
      warn('User %s not found', username)
      reject(new Error('User not found'))
    }
  })
}

const findByLastAuthenticated = () => {
  return new Promise((resolve, reject) => {
    const db = store.getState().db

    if (db && !_.isEmpty(db.conn)) {
      const conn = db.conn
      const usersCollection = conn.collection('users')
      const oneMonthAgo = moment().subtract(1, 'month').valueOf()

      usersCollection.find({lastAuthenticated: {$gt: oneMonthAgo}}).toArray((err, docs) => {
        if (err) {
          error('Unable to load active user docs')
          error(err)
          reject(new Error('Unable to load active user docs'))
        } else if (!_.isEmpty(docs)) {
          resolve(docs)
        } else {
          warn('No active user docs found')
          reject(new Error('No active user docs found'))
        }
      })
    } else {
      warn('Database not valid')
      warn('Unable to load active user docs')
      reject(new Error('Unable to load active user docs'))
    }
  })
}

const updateLastAuthenticated = (username) => {
  return new Promise((resolve, reject) => {
    const db = store.getState().db

    if (db && !_.isEmpty(db.conn)) {
      const conn = db.conn
      const usersCollection = conn.collection('users')

      usersCollection.findOneAndUpdate(
        {username: username},
        {$set: {lastAuthenticated: moment().valueOf()}},
        {},
        (err, doc) => {
          if (err) {
            error('User %s not found: %j', username, err)
            reject(new Error('User not found'))
          } else if (!_.isEmpty(doc)) {
            resolve(doc._id)
          } else {
            warn('User %s not found', username)
            reject(new Error('No user documents found'))
          }
        })
    } else {
      warn('Database not valid')
      warn('User %s not found', username)
      reject(new Error('User not found'))
    }
  })
}

export { findByLastAuthenticated, findByObjWithUsername, findByUsername, findIdByUsername, updateLastAuthenticated }
