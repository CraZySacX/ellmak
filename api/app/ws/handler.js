import jwt from 'jsonwebtoken'
import store from '../redux/store'
import { findByUserId } from '../db/repos'
import { findByUsername } from '../db/users'
import { checkRef, updateInterval } from '../git/refs'
import { sessionActions } from '../redux/sessions'
import { trace } from '../utils/logger'

class WebSocketError extends Error {
  constructor (message) {
    super(message)
    this.name = 'WebSocketError'
    this.message = message
    this.stack = (new Error(message)).stack
  }
}
WebSocketError.prototype = Object.create(Error.prototype)

const createSession = (username, wsKey, ws) => {
  return new Promise((resolve, reject) => {
    const { addWebSocket, createSession } = sessionActions
    store.dispatch(createSession(username))
    store.dispatch(addWebSocket(username, wsKey, ws))
    updateSession(username, true).then(() => resolve()).catch(err => reject(err))
  })
}

const updateMostSession = (username, wsKey, ws) => {
  return new Promise((resolve, reject) => {
    const { addWebSocket } = sessionActions
    store.dispatch(addWebSocket(username, wsKey, ws))
    updateSession(username, false).then(() => resolve()).catch(err => reject(err))
  })
}

const updateSession = (username, check) => {
  return new Promise((resolve, reject) => {
    findByUsername(username).then(userDoc => {
      const { addUserId } = sessionActions
      store.dispatch(addUserId(username, userDoc._id))
      return findByUserId(userDoc._id)
    }).then(repoDocs => {
      repoDocs.forEach(repoDoc => {
        updateInterval(username, repoDoc)
        if (check) {
          checkRef(username, repoDoc)
        }
      })

      resolve()
    }).catch(err => reject(err))
  })
}

const messageHandler = (ws, message, flags) => {
  return new Promise((resolve, reject) => {
    try {
      const parsed = JSON.parse(message)
      const { token, username, msg } = parsed

      if (!username || username.length === 0) {
        reject(new WebSocketError('invalid username'))
      } else {
        jwt.verify(token, process.env.ELLMAK_JWT_SECRET, (err, decoded) => {
          if (err) {
            reject(err)
          } else if (msg === 'authenticated') {
            const wsKey = ws.upgradeReq.headers['sec-websocket-key']
            const session = store.getState().sessions[username]

            if (!session) {
              createSession(username, wsKey, ws).then(() => resolve()).catch(err => reject(err))
            } else {
              updateMostSession(username, wsKey, ws).then(() => resolve()).catch(err => reject(err))
            }
          } else {
            reject(new WebSocketError('unknown message type'))
          }
        })
      }
    } catch (e) {
      reject(e)
    }
  })
}

const closeHandler = (ws) => {
  return new Promise((resolve, reject) => {
    const { removeWebSocket } = sessionActions
    const sessions = store.getState().sessions
    const sessionKeys = Object.keys(sessions)
    const closingKey = ws.upgradeReq.headers['sec-websocket-key']
    var closed = false

    for (const username of sessionKeys) {
      if (closed) {
        break
      } else {
        const webSockets = sessions[username].webSockets
        const webSocketsKeys = Object.keys(webSockets)

        for (const wsKey of webSocketsKeys) {
          if (wsKey === closingKey) {
            trace(`Closing: ${closingKey}`)
            store.dispatch(removeWebSocket(username, wsKey))
            closed = true
            break
          }
        }
      }
    }

    if (closed) {
      resolve()
    } else {
      reject(new Error('Unable to remove websocket from session'))
    }
  })
}

export { closeHandler, messageHandler as default, updateSession }
