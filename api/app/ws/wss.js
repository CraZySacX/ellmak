import store from '../redux/store'
import { wssActions } from '../redux/ws'
import { Server as WebSocketServer } from 'ws'
import messageHandler, { closeHandler } from '../ws/handler'
import { error } from '../utils/logger'

const startWss = (server) => {
  const { addServer } = wssActions
  const wss = new WebSocketServer({server: server})
  store.dispatch(addServer(wss))
  wss.on('connection', (ws, req) => {
    ws.upgradeReq = req
    ws.on('message', (data, flags) => {
      messageHandler(ws, data, flags).catch(err => {
        error(err)
        ws.send(JSON.stringify({name: err.name, msg: err.message, stack: err.stack}))
      })
    })

    ws.on('close', () => {
      closeHandler(ws).catch(err => error(err))
    })
  })
}

const broadcast = (data, callback) => {
  const wss = store.getState().wss
  const { clients } = wss

  if (clients) {
    clients.forEach(client => {
      client.send(data)
    })
  }
}

export { broadcast, startWss }
