module Html.Model exposing (..)


type Attr
    = Aria AriaAttribute
    | Data DataAttribute


type AriaAttribute
    = Controls Id
    | Expanded Bool


type DataAttribute
    = Target Id
    | Toggle ToggleType


type ToggleType
    = Coll
    | Dropdown
    | Modal
    | Tab
    | Tooltip


type Class
    = Bootstrap BootstrapClass
    | Custom CustomClass


type BootstrapClass
    = Btn
    | BtnDefault
    | Col GridAttr
    | Collapse
    | FormControl
    | FormGroup
    | FormHorizontal
    | Glyphicon
    | GlyphiconQuestionSign
    | Offset GridAttr
    | Row
    | TextCenter
    | Well
    | WellSm


type CustomClass
    = ControlLabel
    | PadBottom5
    | SizedTextArea


type Id
    = FrequencyInput
    | FrequencyHelpWell
    | OriginName
    | OriginUrl
    | RemoteInput Int
    | RemoteUrlInput Int
    | RepoRefs
    | RepoRefsHelpWell
    | ShortNameInput
    | ShortNameHelpWell


type InputType
    = Button
    | Text


type Placeholder
    = FreqText
    | Origin
    | RefsTextArea
    | RemoteGithubUrl
    | RemoteId
    | RemoteURL
    | ShortNameText


type GridSize
    = Large
    | Medium
    | Small
    | Xsmall


type alias GridWidth =
    Int


type alias GridAttr =
    { size : GridSize
    , width : GridWidth
    }
