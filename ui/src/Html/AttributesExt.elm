module Html.AttributesExt exposing (..)

import Conversions.Attr exposing (toString)
import Conversions.Class exposing (toString)
import Conversions.Id exposing (toString)
import Conversions.InputType exposing (toString)
import Conversions.Placeholder exposing (toString)
import Html exposing (Attribute)
import Html.Attributes exposing (attribute, class)
import Html.Model exposing (..)
import I18n.Model exposing (I18nId, Language, Region)
import I18n.Utils exposing (translate)


attribute : Attr -> Attribute msg
attribute attr =
    uncurry Html.Attributes.attribute <| Conversions.Attr.toString attr


class : Class -> Attribute msg
class class =
    Html.Attributes.class <| Conversions.Class.toString class


classList : List Class -> Attribute msg
classList classList =
    Html.Attributes.class <| String.join " " <| List.map Conversions.Class.toString classList


for : Id -> Attribute msg
for idVal =
    Html.Attributes.for <| Conversions.Id.toString idVal


id : Id -> Attribute msg
id idVal =
    Html.Attributes.id <| Conversions.Id.toString idVal


placeholder : Placeholder -> Attribute msg
placeholder placeHolder =
    Html.Attributes.placeholder <| Conversions.Placeholder.toString placeHolder


title : Language -> Maybe Region -> I18nId -> Attribute msg
title language region i18nId =
    let
        translatedText =
            translate language region i18nId
    in
        Html.Attributes.title translatedText


type_ : InputType -> Attribute msg
type_ inputType =
    Html.Attributes.type_ <| Conversions.InputType.toString inputType


cl1 : Class
cl1 =
    Bootstrap <| Col { size = Large, width = 1 }


cl2 : Class
cl2 =
    Bootstrap <| Col { size = Large, width = 2 }


cl3 : Class
cl3 =
    Bootstrap <| Col { size = Large, width = 3 }


cl4 : Class
cl4 =
    Bootstrap <| Col { size = Large, width = 4 }


cl5 : Class
cl5 =
    Bootstrap <| Col { size = Large, width = 5 }


cl6 : Class
cl6 =
    Bootstrap <| Col { size = Large, width = 6 }


cl7 : Class
cl7 =
    Bootstrap <| Col { size = Large, width = 7 }


cl8 : Class
cl8 =
    Bootstrap <| Col { size = Large, width = 8 }


cl9 : Class
cl9 =
    Bootstrap <| Col { size = Large, width = 9 }


cl10 : Class
cl10 =
    Bootstrap <| Col { size = Large, width = 10 }


cl11 : Class
cl11 =
    Bootstrap <| Col { size = Large, width = 11 }


cl12 : Class
cl12 =
    Bootstrap <| Col { size = Large, width = 12 }


cm1 : Class
cm1 =
    Bootstrap <| Col { size = Medium, width = 1 }


cm2 : Class
cm2 =
    Bootstrap <| Col { size = Medium, width = 2 }


cm3 : Class
cm3 =
    Bootstrap <| Col { size = Medium, width = 3 }


cm4 : Class
cm4 =
    Bootstrap <| Col { size = Medium, width = 4 }


cm5 : Class
cm5 =
    Bootstrap <| Col { size = Medium, width = 5 }


cm6 : Class
cm6 =
    Bootstrap <| Col { size = Medium, width = 6 }


cm7 : Class
cm7 =
    Bootstrap <| Col { size = Medium, width = 7 }


cm8 : Class
cm8 =
    Bootstrap <| Col { size = Medium, width = 8 }


cm9 : Class
cm9 =
    Bootstrap <| Col { size = Medium, width = 9 }


cm10 : Class
cm10 =
    Bootstrap <| Col { size = Medium, width = 10 }


cm11 : Class
cm11 =
    Bootstrap <| Col { size = Medium, width = 11 }


cm12 : Class
cm12 =
    Bootstrap <| Col { size = Medium, width = 12 }


cs1 : Class
cs1 =
    Bootstrap <| Col { size = Small, width = 1 }


cs2 : Class
cs2 =
    Bootstrap <| Col { size = Small, width = 2 }


cs3 : Class
cs3 =
    Bootstrap <| Col { size = Small, width = 3 }


cs4 : Class
cs4 =
    Bootstrap <| Col { size = Small, width = 4 }


cs5 : Class
cs5 =
    Bootstrap <| Col { size = Small, width = 5 }


cs6 : Class
cs6 =
    Bootstrap <| Col { size = Small, width = 6 }


cs7 : Class
cs7 =
    Bootstrap <| Col { size = Small, width = 7 }


cs8 : Class
cs8 =
    Bootstrap <| Col { size = Small, width = 8 }


cs9 : Class
cs9 =
    Bootstrap <| Col { size = Small, width = 9 }


cs10 : Class
cs10 =
    Bootstrap <| Col { size = Small, width = 10 }


cs11 : Class
cs11 =
    Bootstrap <| Col { size = Small, width = 11 }


cs12 : Class
cs12 =
    Bootstrap <| Col { size = Small, width = 12 }


cxs1 : Class
cxs1 =
    Bootstrap <| Col { size = Xsmall, width = 1 }


cxs2 : Class
cxs2 =
    Bootstrap <| Col { size = Xsmall, width = 2 }


cxs3 : Class
cxs3 =
    Bootstrap <| Col { size = Xsmall, width = 3 }


cxs4 : Class
cxs4 =
    Bootstrap <| Col { size = Xsmall, width = 4 }


cxs5 : Class
cxs5 =
    Bootstrap <| Col { size = Xsmall, width = 5 }


cxs6 : Class
cxs6 =
    Bootstrap <| Col { size = Xsmall, width = 6 }


cxs7 : Class
cxs7 =
    Bootstrap <| Col { size = Xsmall, width = 7 }


cxs8 : Class
cxs8 =
    Bootstrap <| Col { size = Xsmall, width = 8 }


cxs9 : Class
cxs9 =
    Bootstrap <| Col { size = Xsmall, width = 9 }


cxs10 : Class
cxs10 =
    Bootstrap <| Col { size = Xsmall, width = 10 }


cxs11 : Class
cxs11 =
    Bootstrap <| Col { size = Xsmall, width = 11 }


cxs12 : Class
cxs12 =
    Bootstrap <| Col { size = Xsmall, width = 12 }


clo1 : Class
clo1 =
    Bootstrap <| Offset { size = Large, width = 1 }


clo2 : Class
clo2 =
    Bootstrap <| Offset { size = Large, width = 2 }


clo3 : Class
clo3 =
    Bootstrap <| Offset { size = Large, width = 3 }


clo4 : Class
clo4 =
    Bootstrap <| Offset { size = Large, width = 4 }


clo5 : Class
clo5 =
    Bootstrap <| Offset { size = Large, width = 5 }


clo6 : Class
clo6 =
    Bootstrap <| Offset { size = Large, width = 6 }


clo7 : Class
clo7 =
    Bootstrap <| Offset { size = Large, width = 7 }


clo8 : Class
clo8 =
    Bootstrap <| Offset { size = Large, width = 8 }


clo9 : Class
clo9 =
    Bootstrap <| Offset { size = Large, width = 9 }


clo10 : Class
clo10 =
    Bootstrap <| Offset { size = Large, width = 10 }


clo11 : Class
clo11 =
    Bootstrap <| Offset { size = Large, width = 11 }


clo12 : Class
clo12 =
    Bootstrap <| Offset { size = Large, width = 12 }


cmo1 : Class
cmo1 =
    Bootstrap <| Offset { size = Medium, width = 1 }


cmo2 : Class
cmo2 =
    Bootstrap <| Offset { size = Medium, width = 2 }


cmo3 : Class
cmo3 =
    Bootstrap <| Offset { size = Medium, width = 3 }


cmo4 : Class
cmo4 =
    Bootstrap <| Offset { size = Medium, width = 4 }


cmo5 : Class
cmo5 =
    Bootstrap <| Offset { size = Medium, width = 5 }


cmo6 : Class
cmo6 =
    Bootstrap <| Offset { size = Medium, width = 6 }


cmo7 : Class
cmo7 =
    Bootstrap <| Offset { size = Medium, width = 7 }


cmo8 : Class
cmo8 =
    Bootstrap <| Offset { size = Medium, width = 8 }


cmo9 : Class
cmo9 =
    Bootstrap <| Offset { size = Medium, width = 9 }


cmo10 : Class
cmo10 =
    Bootstrap <| Offset { size = Medium, width = 10 }


cmo11 : Class
cmo11 =
    Bootstrap <| Offset { size = Medium, width = 11 }


cmo12 : Class
cmo12 =
    Bootstrap <| Offset { size = Medium, width = 12 }


cso1 : Class
cso1 =
    Bootstrap <| Offset { size = Small, width = 1 }


cso2 : Class
cso2 =
    Bootstrap <| Offset { size = Small, width = 2 }


cso3 : Class
cso3 =
    Bootstrap <| Offset { size = Small, width = 3 }


cso4 : Class
cso4 =
    Bootstrap <| Offset { size = Small, width = 4 }


cso5 : Class
cso5 =
    Bootstrap <| Offset { size = Small, width = 5 }


cso6 : Class
cso6 =
    Bootstrap <| Offset { size = Small, width = 6 }


cso7 : Class
cso7 =
    Bootstrap <| Offset { size = Small, width = 7 }


cso8 : Class
cso8 =
    Bootstrap <| Offset { size = Small, width = 8 }


cso9 : Class
cso9 =
    Bootstrap <| Offset { size = Small, width = 9 }


cso10 : Class
cso10 =
    Bootstrap <| Offset { size = Small, width = 10 }


cso11 : Class
cso11 =
    Bootstrap <| Offset { size = Small, width = 11 }


cso12 : Class
cso12 =
    Bootstrap <| Offset { size = Small, width = 12 }


cxso1 : Class
cxso1 =
    Bootstrap <| Offset { size = Xsmall, width = 1 }


cxso2 : Class
cxso2 =
    Bootstrap <| Offset { size = Xsmall, width = 2 }


cxso3 : Class
cxso3 =
    Bootstrap <| Offset { size = Xsmall, width = 3 }


cxso4 : Class
cxso4 =
    Bootstrap <| Offset { size = Xsmall, width = 4 }


cxso5 : Class
cxso5 =
    Bootstrap <| Offset { size = Xsmall, width = 5 }


cxso6 : Class
cxso6 =
    Bootstrap <| Offset { size = Xsmall, width = 6 }


cxso7 : Class
cxso7 =
    Bootstrap <| Offset { size = Xsmall, width = 7 }


cxso8 : Class
cxso8 =
    Bootstrap <| Offset { size = Xsmall, width = 8 }


cxso9 : Class
cxso9 =
    Bootstrap <| Offset { size = Xsmall, width = 9 }


cxso10 : Class
cxso10 =
    Bootstrap <| Offset { size = Xsmall, width = 10 }


cxso11 : Class
cxso11 =
    Bootstrap <| Offset { size = Xsmall, width = 11 }


cxso12 : Class
cxso12 =
    Bootstrap <| Offset { size = Xsmall, width = 12 }
