module Ws.Model exposing (..)


type alias JsError =
    { name : String
    , message : String
    , stack : String
    }


type alias Repo =
    { msgType : String
    , subject : String
    , version : String
    , uname : String
    , refs : List Ref
    }


type alias Ref =
    { ref : String
    , lastUpdated : String
    }


type WebSocketMessage
    = Message String
    | RepoMessage Repo
    | ErrorMessage JsError
