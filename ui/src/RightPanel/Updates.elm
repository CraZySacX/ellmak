module RightPanel.Updates exposing (..)

import Dict exposing (filter)
import Navigation exposing (newUrl)
import Repo.Model exposing (Repository)
import RightPanel.Messages exposing (..)
import RightPanel.Model exposing (RightPanel)


filterBySearchText : String -> String -> Repository -> Bool
filterBySearchText searchText shortName repo =
    String.contains searchText shortName


update : InternalMsg -> RightPanel -> ( RightPanel, Cmd Msg )
update msg model =
    case msg of
        SearchRepositories searchText ->
            let
                filteredRepos =
                    Dict.filter (filterBySearchText searchText) model.repos
            in
                ( { model | filteredRepos = filteredRepos }, Cmd.none )

        ToAdd ->
            ( model, newUrl ("#addrepo") )

        ToEdit ->
            ( model, newUrl ("#editrepo") )

        NoOp ->
            ( model, Cmd.none )
