module RightPanel.View exposing (..)

import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Repo.Model exposing (Repository)
import RightPanel.Messages exposing (..)
import RightPanel.Model exposing (RightPanel)


panelContent : String -> List (Html a) -> Html a
panelContent heading body =
    div [ class "panel panel-default" ]
        [ div [ class "panel-heading" ] [ text heading ]
        , div [ class "panel-body" ] body
        ]


repoPanel : Int -> ( String, Repository ) -> Html Msg
repoPanel idx ( shortName, repo ) =
    let
        idStr =
            "heading-" ++ shortName ++ "-" ++ (toString idx)

        collStr =
            "collapse-" ++ shortName ++ "-" ++ (toString idx)

        hrefStr =
            "#" ++ collStr
    in
        div [ class "panel panel-default" ]
            [ div
                [ id idStr
                , class "clickable panel-heading"
                , attribute "role" "tab"
                , attribute "data-toggle" "collapse"
                , attribute "data-parent" "#right-accordion"
                , attribute "data-target" hrefStr
                ]
                [ h4 [ class "panel-title" ]
                    [ a
                        [ attribute "role" "button"
                        , attribute "data-toggle" "collapse"
                        , attribute "data-parent" "#right-accordion"
                        , href hrefStr
                        , attribute "aria-expanded" "true"
                        , attribute "aria-controls" collStr
                        ]
                        [ text shortName ]
                    ]
                ]
            , div [ class "panel-collapse collapse", id collStr ]
                [ div [ class "panel-body" ]
                    [ button
                        [ class "btn btn-default"
                        , attribute "data-toggle" "tooltip"
                        , title "Edit Repository"
                        , onClick <| ForSelf ToEdit
                        ]
                        [ span [ class "glyphicon glyphicon-cog" ] []
                        ]
                    , button
                        [ class "btn btn-default"
                        , attribute "data-toggle" "tooltip"
                        , title "Remove Repository"
                        , onClick <| ForParent <| ToRemove shortName
                        ]
                        [ span [ class "glyphicon glyphicon-remove" ] []
                        ]
                    ]
                ]
            ]


view : RightPanel -> Html Msg
view model =
    panelContent "Repository Management"
        [ div [ class "row" ]
            [ div [ class "col-lg-12 col-md-12 col-sm-12 col-xs-12" ]
                [ Html.form [ onSubmit <| ForSelf NoOp ]
                    [ div [ class "form-group" ]
                        [ div [ class "input-group" ]
                            [ input
                                [ type_ "text"
                                , class "form-control"
                                , placeholder "Search Repositories"
                                , onInput <| ForSelf << SearchRepositories
                                ]
                                []
                            , div
                                [ class "clickable input-group-addon"
                                , attribute "data-toggle" "tooltip"
                                , title "Add Repository"
                                , onClick <| ForSelf ToAdd
                                ]
                                [ span [ class "glyphicon glyphicon-plus-sign" ] []
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        , div [ class "row" ]
            [ div [ class "col-lg-12 col-md-12 col-sm-12 col-xs-12" ]
                [ div
                    [ class "panel-group"
                    , id "right-accordion"
                    , attribute "role" "tablist"
                    , attribute "aria-multiselectable" "true"
                    ]
                    (List.indexedMap repoPanel <| Dict.toList model.filteredRepos)
                ]
            ]
        ]
