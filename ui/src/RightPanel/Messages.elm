module RightPanel.Messages exposing (..)


type InternalMsg
    = ToAdd
    | ToEdit
    | SearchRepositories String
    | NoOp


type ExternalMsg
    = ToRemove String


type Msg
    = ForSelf InternalMsg
    | ForParent ExternalMsg


type alias TranslationDictionary msg =
    { onInternalMessage : InternalMsg -> msg
    , onRemoveRepo : String -> msg
    }


type alias Translator parentMsg =
    Msg -> parentMsg


translator : TranslationDictionary parentMsg -> Translator parentMsg
translator { onInternalMessage, onRemoveRepo } msg =
    case msg of
        ForSelf internal ->
            onInternalMessage internal

        ForParent (ToRemove repo) ->
            onRemoveRepo repo
