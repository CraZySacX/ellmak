module Conversions.Placeholder exposing (toString)

import Html.Model exposing (Placeholder(..))


toString : Placeholder -> String
toString placeholder =
    case placeholder of
        FreqText ->
            "15m"

        Origin ->
            "origin"

        RefsTextArea ->
            "origin/master\nrefs/heads/github/master\nrefs/heads/github/cool-feature"

        RemoteGithubUrl ->
            "git@github.com:CraZySacX/ellmak.git"

        RemoteId ->
            "github"

        RemoteURL ->
            "https://github.com/CraZySacX/ellmak.git"

        ShortNameText ->
            "ellmak"
