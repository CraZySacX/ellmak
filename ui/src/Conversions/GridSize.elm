module Conversions.GridSize exposing (toString)

import Html.Model exposing (GridSize(..))


toString : GridSize -> String
toString sz =
    case sz of
        Large ->
            "lg"

        Medium ->
            "md"

        Small ->
            "sm"

        Xsmall ->
            "xs"
