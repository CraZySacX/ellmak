module Conversions.ToggleType exposing (toString)

import Html.Model exposing (ToggleType(..))


toString : ToggleType -> String
toString toggleType =
    case toggleType of
        Coll ->
            "collapse"

        Dropdown ->
            "dropdown"

        Modal ->
            "modal"

        Tab ->
            "tab"

        Tooltip ->
            "tooltip"
