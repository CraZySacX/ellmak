module Conversions.Language exposing (toString)

import I18n.Model exposing (Language(..), Region(..))


toString : Language -> Maybe Region -> String
toString language region =
    case ( language, region ) of
        ( English, Nothing ) ->
            "en"

        ( English, Just UnitedStates ) ->
            "en-US"

        ( French, Nothing ) ->
            "fr"

        ( French, Just France ) ->
            "fr-FR"

        ( Spanish, Nothing ) ->
            "es"

        ( Spanish, Just Spain ) ->
            "es-ES"

        ( _, _ ) ->
            "en-US"
