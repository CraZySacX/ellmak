module Conversions.Id exposing (toSelector, toString)

import Html.Model exposing (Id(..))


toString : Id -> String
toString idv =
    case idv of
        FrequencyHelpWell ->
            "frequency-help-well"

        FrequencyInput ->
            "frequency"

        OriginName ->
            "origin-name"

        OriginUrl ->
            "origin-url"

        RemoteInput idx ->
            "remote-" ++ (Basics.toString idx)

        RemoteUrlInput idx ->
            "remote-" ++ (Basics.toString idx) ++ "-url"

        RepoRefs ->
            "repo-refs"

        RepoRefsHelpWell ->
            "repo-refs-help-well"

        ShortNameHelpWell ->
            "short-name-help-well"

        ShortNameInput ->
            "short-name"


toSelector : Id -> String
toSelector idv =
    String.append "#" <| toString idv
