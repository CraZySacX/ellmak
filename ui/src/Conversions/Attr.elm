module Conversions.Attr exposing (toString)

import Conversions.Id exposing (toSelector, toString)
import Conversions.ToggleType exposing (toString)
import Html.Model exposing (Attr(..), AriaAttribute(..), DataAttribute(..))


toString : Attr -> ( String, String )
toString attr =
    case attr of
        Aria aria ->
            let
                prefix =
                    "aria-"
            in
                case aria of
                    Controls val ->
                        ( prefix ++ "control", Conversions.Id.toString val )

                    Expanded val ->
                        ( prefix ++ "expanded"
                        , if val then
                            "true"
                          else
                            "false"
                        )

        Data data ->
            let
                prefix =
                    "data-"
            in
                case data of
                    Toggle val ->
                        ( prefix ++ "toggle", Conversions.ToggleType.toString val )

                    Target val ->
                        ( prefix ++ "target", Conversions.Id.toSelector val )
