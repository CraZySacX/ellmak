module Conversions.String exposing (..)

import Env.Env exposing (Environment(..))
import I18n.Model exposing (Language(..), Region(..))
import Routing.Router exposing (Route(..))


toRoute : String -> Route
toRoute str =
    case str of
        "" ->
            Home

        "Home" ->
            Home

        "AddRepo" ->
            AddRepo

        "RemoveRepo" ->
            RemoveRepo

        _ ->
            NotFound


toEnvironment : String -> Environment
toEnvironment env =
    case env of
        "development" ->
            Development

        "integration" ->
            Integration

        "staging" ->
            Staging

        "production" ->
            Production

        _ ->
            Development


toLanguageRegion : String -> ( Language, Maybe Region )
toLanguageRegion languageTag =
    case languageTag of
        "en" ->
            ( English, Nothing )

        "en-US" ->
            ( English, Just UnitedStates )

        "fr" ->
            ( French, Nothing )

        "fr-FR" ->
            ( French, Just France )

        "es" ->
            ( Spanish, Nothing )

        "es-ES" ->
            ( Spanish, Just Spain )

        _ ->
            ( English, Just UnitedStates )
