module Conversions.InputType exposing (toString)

import Html.Model exposing (InputType(..))


toString : InputType -> String
toString inputType =
    case inputType of
        Button ->
            "button"

        Text ->
            "text"
