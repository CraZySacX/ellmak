module Conversion.WsMsg exposing (..)

import Ws.Model exposing (WebSocketMessage)


toString : WebSocketMessage -> String
toString msg =
    case msg of
        Ws.Model.ErrorMessage err ->
            toString msg

        _ ->
            toString msg
