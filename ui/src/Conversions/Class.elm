module Conversions.Class exposing (toString)

import Conversions.GridSize exposing (toString)
import Html.Model exposing (..)


toString : Class -> String
toString class =
    case class of
        Bootstrap bootstrapClass ->
            case bootstrapClass of
                Btn ->
                    "btn"

                BtnDefault ->
                    "btn-default"

                Col { size, width } ->
                    String.join "-" [ "col", (Conversions.GridSize.toString size), (Basics.toString width) ]

                Collapse ->
                    "collapse"

                FormControl ->
                    "form-control"

                FormGroup ->
                    "form-group"

                FormHorizontal ->
                    "form-horizontal"

                Glyphicon ->
                    "glyphicon"

                GlyphiconQuestionSign ->
                    "glyphicon-question-sign"

                Offset { size, width } ->
                    String.join "-" [ "col", (Conversions.GridSize.toString size), "offset", (Basics.toString width) ]

                Row ->
                    "row"

                TextCenter ->
                    "text-center"

                Well ->
                    "well"

                WellSm ->
                    "well-sm"

        Custom customClass ->
            case customClass of
                ControlLabel ->
                    "control-label"

                PadBottom5 ->
                    "pad-bottom-5"

                SizedTextArea ->
                    "sized-text-area"
