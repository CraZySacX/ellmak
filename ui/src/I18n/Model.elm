module I18n.Model exposing (I18nId(..), Language(..), Region(..))


type I18nId
    = Frequency
    | Help
    | Origin
    | RefsToMonitor
    | RemoteName
    | RemoteURL
    | ShortName


type Language
    = English
    | French
    | Spanish


type Region
    = France
    | Spain
    | UnitedStates
