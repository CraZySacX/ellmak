module I18n.Utils exposing (translate)

import I18n.Model exposing (I18nId(..), Language(..), Region(..))


type alias TranslationSet =
    { en : String
    , enUS : String
    , es : String
    , esES : String
    , fr : String
    , frFR : String
    }


frequency : TranslationSet
frequency =
    { en = "Frequency"
    , enUS = "Frequency"
    , es = "Frequency (ntx)"
    , esES = "Frequency (ntx)"
    , fr = "Frequency (ntx)"
    , frFR = "Frequency (ntx)"
    }


help : TranslationSet
help =
    { en = "Help"
    , enUS = "Help"
    , es = "Help (ntx)"
    , esES = "Help (ntx)"
    , fr = "Help (ntx)"
    , frFR = "Help (ntx)"
    }


origin : TranslationSet
origin =
    { en = "Origin"
    , enUS = "Origin"
    , es = "Oregen"
    , esES = "Oregen"
    , fr = "Origine"
    , frFR = "Origine"
    }


refsToMonitor : TranslationSet
refsToMonitor =
    { en = "References To Monitor"
    , enUS = "References To Monitor"
    , es = "References To Monitor (ntx)"
    , esES = "References To Monitor (ntx)"
    , fr = "References To Monitor (ntx)"
    , frFR = "References To Monitor (ntx)"
    }


remoteName : TranslationSet
remoteName =
    { en = "Remote Name"
    , enUS = "Remote Name"
    , es = "Nombre Remoto"
    , esES = "Nombre Remoto"
    , fr = "Nom Distant"
    , frFR = "Nom Distant"
    }


remoteURL : TranslationSet
remoteURL =
    { en = "Remote URL"
    , enUS = "Remote URL"
    , es = "URL Remota"
    , esES = "URL Remota"
    , fr = "URL distante"
    , frFR = "URL distante"
    }


shortName : TranslationSet
shortName =
    { en = "Short Name"
    , enUS = "Short Name"
    , es = "Short Name (ntx)"
    , esES = "Short Name (ntx)"
    , fr = "Short Name (ntx)"
    , frFR = "Short Name (ntx)"
    }


translate : Language -> Maybe Region -> I18nId -> String
translate language region i18nId =
    let
        translationSet =
            case i18nId of
                Frequency ->
                    frequency

                Help ->
                    help

                Origin ->
                    origin

                RefsToMonitor ->
                    refsToMonitor

                RemoteName ->
                    remoteName

                RemoteURL ->
                    remoteURL

                ShortName ->
                    shortName
    in
        case ( language, region ) of
            ( English, Just UnitedStates ) ->
                .enUS translationSet

            ( English, Nothing ) ->
                .en translationSet

            ( French, Just France ) ->
                .frFR translationSet

            ( French, Nothing ) ->
                .fr translationSet

            ( Spanish, Just Spain ) ->
                .esES translationSet

            ( Spanish, Nothing ) ->
                .es translationSet

            ( _, _ ) ->
                .enUS translationSet
