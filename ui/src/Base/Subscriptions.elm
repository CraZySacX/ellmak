module Base.Subscriptions exposing (..)

import Auth.Subscriptions
import Base.Model exposing (BaseModel)
import Base.Messages exposing (..)
import LeftPanel.Subscriptions
import WebSocket


webSocketListener : BaseModel -> Sub BaseMsg
webSocketListener model =
    WebSocket.listen model.wsBaseUrl NewMessage


subscriptions : BaseModel -> Sub BaseMsg
subscriptions model =
    case model.authentication.authenticated of
        False ->
            Sub.batch
                [ Sub.map AuthMsg (Auth.Subscriptions.subscriptions model.authentication)
                , Sub.map LeftPanelMsg (LeftPanel.Subscriptions.subscriptions model.leftPanel)
                ]

        True ->
            Sub.batch
                [ webSocketListener model
                , Sub.map AuthMsg (Auth.Subscriptions.subscriptions model.authentication)
                , Sub.map LeftPanelMsg (LeftPanel.Subscriptions.subscriptions model.leftPanel)
                ]
