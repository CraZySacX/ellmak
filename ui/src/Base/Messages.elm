module Base.Messages exposing (..)

import Auth.Model exposing (AuthError, JwtPayload)
import Auth.Messages exposing (InternalMsg)
import Base.Model exposing (AlertifyConfig, RemoveResult)
import Http
import I18n.Model exposing (Language, Region)
import LeftPanel.Messages exposing (InternalMsg)
import LeftPanel.Model exposing (LeftPanel)
import Navbar.Messages exposing (..)
import Navigation exposing (Location)
import Repo.Model exposing (Repository)
import RightPanel.Messages exposing (..)
import Time exposing (Time)


type BaseMsg
    = -- Parent to Child Messages
      AuthMsg Auth.Messages.InternalMsg
    | LeftPanelMsg LeftPanel.Messages.InternalMsg
    | NavMsg Navbar.Messages.NavbarMsg
    | RightPanelMsg RightPanel.Messages.InternalMsg
      -- Child to Parent Messages
    | AuthError Auth.Model.AuthError
    | AuthSuccess
    | PostRepo LeftPanel.Model.LeftPanel
    | ToRemove String
    | RemoveRepo String
    | SendWsMessage String
      -- Navigation Messages
    | LocationChange Location
    | ToHome
    | ToAdd
      -- UI Messages
    | Alert AlertifyConfig
    | Clone
    | Repo String
    | Eat
    | NoOp
    | SetLanguageRegion ( Language, Maybe Region )
      -- API Messages
    | CloneRequest (Result Http.Error String)
    | PostRepoResult (Result Http.Error Repository)
    | GetRepoResult (Result Http.Error (List Repository))
    | RemoveRepoResult (Result Http.Error RemoveResult)
      -- WebSocket Listener
    | NewMessage String
    | TimestampMessage String Time
