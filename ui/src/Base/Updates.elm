module Base.Updates exposing (..)

import Auth.Messages exposing (..)
import Auth.Model exposing (AuthError(..))
import Auth.Updates exposing (authenticated, fromDecodeResult, tokenPayloadDecoder)
import Base.Messages exposing (..)
import Base.Model exposing (..)
import Conversions.Model exposing (toFlags)
import Dict exposing (toList)
import Http exposing (..)
import I18n.Model exposing (Language, Region)
import Json.Decode as Decode exposing (..)
import Json.Encode as Encode exposing (..)
import Jwt exposing (decodeToken, JwtError(TokenProcessingError, TokenDecodeError))
import LeftPanel.Messages exposing (Translator, translator)
import LeftPanel.Model exposing (LeftPanel)
import LeftPanel.Updates exposing (update)
import Navigation exposing (..)
import Navbar.Updates exposing (..)
import Ports.Ports exposing (alertify, collapseAll, storeFlags)
import Random.Pcg exposing (step)
import Repo.Model exposing (Reference, Repository)
import RightPanel.Messages exposing (Translator, translator)
import RightPanel.Updates exposing (update)
import Routing.Router exposing (..)
import Task exposing (perform)
import Time exposing (now)
import Uuid exposing (uuidGenerator, Uuid)
import WebSocket
import Ws.Model exposing (..)


storeFlags : BaseModel -> Cmd BaseMsg
storeFlags model =
    toFlags model |> Ports.Ports.storeFlags


showAlert : BaseModel -> String -> String -> ( BaseModel, Cmd BaseMsg )
showAlert model level message =
    let
        baseConfig =
            Base.Model.newConfig

        config =
            { baseConfig | message = message, logType = level }
    in
        ( model, alertCmd config )


cloneDecoder : Decoder String
cloneDecoder =
    field "clone" Decode.string


clone : BaseModel -> Cmd BaseMsg
clone model =
    let
        request =
            Http.request
                { method = "GET"
                , headers = [ (Http.header "Authorization" ("Bearer " ++ model.authentication.token)) ]
                , url = (model.baseUrl ++ "/clone/?repo=blah")
                , body = Http.emptyBody
                , expect = Http.expectJson cloneDecoder
                , timeout = Nothing
                , withCredentials = True
                }
    in
        Http.send CloneRequest request


alertCmd : AlertifyConfig -> Cmd BaseMsg
alertCmd config =
    alertify config


authTranslator : Auth.Messages.Translator BaseMsg
authTranslator =
    Auth.Messages.translator { onInternalMessage = AuthMsg, onAuthSuccess = AuthSuccess, onAuthError = AuthError }


leftPanelTranslator : LeftPanel.Messages.Translator BaseMsg
leftPanelTranslator =
    LeftPanel.Messages.translator
        { onInternalMessage = LeftPanelMsg
        , onPostRepo = Base.Messages.PostRepo
        , onRemoveRepo = Base.Messages.RemoveRepo
        , onSendWsMessage = SendWsMessage
        }


rightPanelTranslator : RightPanel.Messages.Translator BaseMsg
rightPanelTranslator =
    RightPanel.Messages.translator
        { onInternalMessage = RightPanelMsg
        , onRemoveRepo = Base.Messages.ToRemove
        }


checkExpiry : BaseModel -> Cmd BaseMsg
checkExpiry model =
    Cmd.map authTranslator (authenticated model.authentication)


messageEncoder : BaseModel -> String -> Encode.Value
messageEncoder model message =
    let
        uuidStr =
            case model.uuid of
                Just uuid ->
                    (Uuid.toString uuid)

                Nothing ->
                    ""
    in
        Encode.object
            [ ( "token", Encode.string model.authentication.token )
            , ( "username", Encode.string model.authentication.username )
            , ( "uuid", Encode.string uuidStr )
            , ( "msg", Encode.string message )
            ]


refsDecoder : Decoder Ref
refsDecoder =
    Decode.map2 Ref
        (field "ref" Decode.string)
        (field "lastUpdatedTime" Decode.string)


repoMessageDecoder : Decoder WebSocketMessage
repoMessageDecoder =
    Decode.map RepoMessage <|
        Decode.map5 Ws.Model.Repo
            (field "msgType" Decode.string)
            (field "subject" Decode.string)
            (field "version" Decode.string)
            (field "uname" Decode.string)
            (at [ "refs" ] (Decode.list refsDecoder))


errorDecoder : Decoder WebSocketMessage
errorDecoder =
    Decode.map ErrorMessage <|
        Decode.map3 JsError
            (field "name" Decode.string)
            (field "msg" Decode.string)
            (field "stack" Decode.string)


messageDecoder : Decoder WebSocketMessage
messageDecoder =
    oneOf [ repoMessageDecoder, Decode.map Message Decode.string, errorDecoder ]


repoSuffix : String
repoSuffix =
    "/repo"


remoteEncoder : String -> Encode.Value
remoteEncoder branch =
    Encode.object
        [ ( "ref", Encode.string branch )
        , ( "lastUpdated", Encode.float 0 )
        , ( "flagged", Encode.bool False )
        ]


repoEncoder : LeftPanel -> String -> Encode.Value
repoEncoder model username =
    let
        additionalRemotesValues =
            Dict.values model.addRemotesDict

        remotesToAppend =
            Dict.fromList additionalRemotesValues

        allRemotes =
            Dict.union model.remotesDict remotesToAppend
    in
        Encode.object
            [ ( "remotes", Encode.object <| Dict.toList <| Dict.map (\k v -> Encode.string v) allRemotes )
            , ( "refs", Encode.list (List.map remoteEncoder model.branches) )
            , ( "frequency", Encode.string model.frequency )
            , ( "shortName", Encode.string model.shortName )
            , ( "username", Encode.string username )
            ]


removeRepoEncoder : String -> Encode.Value
removeRepoEncoder shortName =
    Encode.object
        [ ( "shortName", Encode.string shortName )
        ]


remoteDecoder : Decoder Reference
remoteDecoder =
    map2 Reference
        (field "ref" Decode.string)
        (field "lastUpdated" Decode.float)


repoDecoder : Decoder Repository
repoDecoder =
    map4 Repository
        (field "remotes" (Decode.dict Decode.string))
        (field "refs" (Decode.list remoteDecoder))
        (field "frequency" Decode.string)
        (field "shortName" Decode.string)


reposDecoder : Decoder (List Repository)
reposDecoder =
    Decode.list repoDecoder


removeReposDecoder : Decoder RemoveResult
removeReposDecoder =
    map2 RemoveResult
        (field "n" Decode.int)
        (field "ok" Decode.int)


addRepo : BaseModel -> LeftPanel -> Cmd BaseMsg
addRepo model leftPanelModel =
    let
        request =
            Http.request
                { method = "POST"
                , headers = [ (Http.header "Authorization" ("Bearer " ++ model.authentication.token)) ]
                , url = (model.baseUrl ++ repoSuffix)
                , body = Http.jsonBody <| repoEncoder leftPanelModel model.authentication.username
                , expect = Http.expectJson repoDecoder
                , timeout = Nothing
                , withCredentials = True
                }
    in
        Http.send PostRepoResult request


getRepos : BaseModel -> Cmd BaseMsg
getRepos model =
    let
        request =
            Http.request
                { method = "GET"
                , headers = [ (Http.header "Authorization" ("Bearer " ++ model.authentication.token)) ]
                , url = (model.baseUrl ++ repoSuffix ++ "?username=" ++ model.authentication.username)
                , body = Http.emptyBody
                , expect = Http.expectJson reposDecoder
                , timeout = Nothing
                , withCredentials = True
                }
    in
        Http.send GetRepoResult request


removeRepo : BaseModel -> Cmd BaseMsg
removeRepo model =
    let
        request =
            Http.request
                { method = "DELETE"
                , headers = [ (Http.header "Authorization" ("Bearer " ++ model.authentication.token)) ]
                , url = (model.baseUrl ++ repoSuffix)
                , body = Http.jsonBody <| removeRepoEncoder model.leftPanel.repoToRemove
                , expect = Http.expectJson removeReposDecoder
                , timeout = Nothing
                , withCredentials = True
                }
    in
        Http.send RemoveRepoResult request


sendMessage : String -> Cmd BaseMsg
sendMessage message =
    Task.perform SendWsMessage <| Task.succeed message


setLanguageRegion : Language -> Maybe Region -> Cmd BaseMsg
setLanguageRegion language region =
    Cmd.batch
        [ Task.perform SetLanguageRegion <| Task.succeed ( language, region )
        , Cmd.map leftPanelTranslator <| LeftPanel.Updates.setLanguageRegion language region
        ]


update : BaseMsg -> BaseModel -> ( BaseModel, Cmd BaseMsg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Alert config ->
            ( model, alertCmd config )

        Repo repo ->
            showAlert model "success" repo

        CloneRequest result ->
            case result of
                Ok success ->
                    ( model, Cmd.none )

                Err err ->
                    showAlert model "error" (toString err)

        LeftPanelMsg subMsg ->
            let
                ( updatedModel, leftPanelCmd ) =
                    LeftPanel.Updates.update subMsg model.leftPanel

                newModel =
                    { model | leftPanel = updatedModel }
            in
                ( newModel, Cmd.batch [ storeFlags newModel, Cmd.map leftPanelTranslator leftPanelCmd ] )

        PostRepo leftPanelModel ->
            ( model, addRepo model leftPanelModel )

        PostRepoResult result ->
            case result of
                Ok success ->
                    let
                        rightPanel =
                            model.rightPanel

                        repos =
                            rightPanel.repos

                        reposDict =
                            Dict.insert success.shortName success repos

                        newRightPanel =
                            { rightPanel | repos = reposDict, filteredRepos = reposDict }

                        newLeftPanel =
                            LeftPanel.Model.defaultLeftPanel

                        newModel =
                            { model | rightPanel = newRightPanel, leftPanel = newLeftPanel }
                    in
                        ( newModel, Cmd.batch [ storeFlags newModel, newUrl "#" ] )

                Err err ->
                    showAlert model "error" (toString err)

        GetRepoResult result ->
            case result of
                Ok success ->
                    let
                        rightPanel =
                            model.rightPanel

                        repos =
                            rightPanel.repos

                        reposDict =
                            Dict.fromList <| List.map (\x -> ( x.shortName, x )) success

                        newRightPanel =
                            { rightPanel | repos = reposDict, filteredRepos = reposDict }

                        newModel =
                            { model | rightPanel = newRightPanel }
                    in
                        ( newModel, storeFlags newModel )

                Err err ->
                    showAlert model "error" (toString err)

        RightPanelMsg subMsg ->
            let
                ( updatedModel, rightPanelCmd ) =
                    RightPanel.Updates.update subMsg model.rightPanel

                newModel =
                    { model | rightPanel = updatedModel }
            in
                ( newModel, Cmd.batch [ storeFlags newModel, Cmd.map rightPanelTranslator rightPanelCmd ] )

        AuthMsg subMsg ->
            let
                ( updatedAuthModel, authCmd ) =
                    Auth.Updates.update subMsg model.authentication model.baseUrl

                newModel =
                    { model | authentication = updatedAuthModel }
            in
                ( newModel, Cmd.batch [ storeFlags newModel, Cmd.map authTranslator authCmd ] )

        AuthSuccess ->
            ( model, Cmd.batch [ getRepos model, sendMessage "authenticated" ] )

        AuthError error ->
            case error of
                HttpError httpError ->
                    case httpError of
                        BadStatus resp ->
                            let
                                message =
                                    (toString resp.status.code)
                                        ++ " "
                                        ++ resp.status.message
                                        ++ ": "
                                        ++ resp.body
                            in
                                showAlert model "error" message

                        BadPayload debug resp ->
                            let
                                message =
                                    (toString resp.status.code)
                                        ++ " "
                                        ++ resp.status.message
                                        ++ ": "
                                        ++ resp.body
                                        ++ ": "
                                        ++ debug
                            in
                                showAlert model "error" message

                        BadUrl url ->
                            let
                                message =
                                    "Bad Url: " ++ url
                            in
                                showAlert model "error" message

                        _ ->
                            showAlert model "error" (toString httpError)

                TokenError tokenError ->
                    case tokenError of
                        TokenProcessingError tpe ->
                            showAlert model "error" tpe

                        TokenDecodeError tde ->
                            showAlert model "error" tde

                        _ ->
                            showAlert model "error" (toString tokenError)

        NavMsg subMsg ->
            let
                ( unm, ncmd ) =
                    Navbar.Updates.update subMsg model
            in
                ( unm, Cmd.batch [ storeFlags unm, Cmd.map NavMsg ncmd ] )

        ToHome ->
            ( model, Navigation.newUrl ("#") )

        ToAdd ->
            ( model, Navigation.newUrl ("#addrepo") )

        Clone ->
            ( model, clone model )

        LocationChange location ->
            let
                currentRoute =
                    routeFromMaybe <| hashParser location

                { leftPanel } =
                    model

                newLeftPanel =
                    { leftPanel | route = currentRoute }

                newModel =
                    { model | leftPanel = newLeftPanel }
            in
                ( newModel, storeFlags newModel )

        Eat ->
            ( model, Cmd.none )

        TimestampMessage message time ->
            let
                ( uuid, nextSeed ) =
                    step uuidGenerator model.currentSeed

                { leftPanel } =
                    model

                newLeftPanel =
                    case (decodeString messageDecoder message) of
                        Ok msg ->
                            { leftPanel | messages = Dict.insert (Uuid.toString uuid) ( time, msg ) leftPanel.messages }

                        Err err ->
                            leftPanel

                newModel =
                    { model | leftPanel = newLeftPanel, currentSeed = nextSeed }
            in
                ( newModel, Cmd.none )

        NewMessage message ->
            model ! [ Task.perform (TimestampMessage message) now ]

        SendWsMessage message ->
            ( model, WebSocket.send model.wsBaseUrl <| encode 0 <| messageEncoder model message )

        ToRemove repo ->
            let
                { leftPanel } =
                    model

                newLeftPanel =
                    { leftPanel | repoToRemove = repo }
            in
                ( { model | leftPanel = newLeftPanel }, newUrl ("#removerepo") )

        Base.Messages.RemoveRepo repo ->
            ( model, removeRepo model )

        RemoveRepoResult result ->
            case result of
                Ok success ->
                    ( model, Cmd.batch [ collapseAll (), getRepos model, newUrl ("#") ] )

                Err err ->
                    showAlert model "error" (toString err)

        SetLanguageRegion ( language, region ) ->
            let
                newModel =
                    { model | language = language, region = region }
            in
                newModel ! [ storeFlags newModel ]
