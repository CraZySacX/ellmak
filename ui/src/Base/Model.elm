module Base.Model exposing (..)

import Auth.Model exposing (Authentication, defaultAuthentication)
import Env.Env exposing (Environment(Development))
import I18n.Model exposing (Language(..), Region(..))
import LeftPanel.Model exposing (LeftPanel, defaultLeftPanel)
import Random.Pcg exposing (initialSeed, Seed)
import RightPanel.Model exposing (RightPanel, defaultRightPanel)
import Uuid exposing (Uuid)


type alias AlertifyConfig =
    { message : String
    , position : String
    , maxItems : Int
    , closeDelay : Int
    , cloc : Bool
    , logType : String
    }


newConfig : AlertifyConfig
newConfig =
    { message = ""
    , position = "bottom center"
    , maxItems = 2
    , closeDelay = 3000
    , cloc = False
    , logType = "success"
    }


type alias BaseModel =
    { env : Environment
    , currentSeed : Seed
    , uuid : Maybe Uuid
    , baseUrl : String
    , wsBaseUrl : String
    , apiVersion : String
    , uiVersion : String
    , authentication : Authentication
    , leftPanel : LeftPanel
    , rightPanel : RightPanel
    , language : Language
    , region : Maybe Region
    }


defaultBase : BaseModel
defaultBase =
    { env = Development
    , currentSeed = initialSeed 0
    , uuid = Nothing
    , baseUrl = "http://localhost:3000/api/v1"
    , wsBaseUrl = "http://localhost:3000"
    , apiVersion = ""
    , uiVersion = ""
    , authentication = defaultAuthentication
    , leftPanel = defaultLeftPanel
    , rightPanel = defaultRightPanel
    , language = English
    , region = Just UnitedStates
    }


type alias RemoveResult =
    { count : Int
    , status : Int
    }
