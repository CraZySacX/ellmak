module LeftPanel.Model exposing (..)

import Dict exposing (Dict, empty)
import I18n.Model exposing (Language(..), Region(..))
import Routing.Router exposing (Route(Home))
import Time exposing (Time)
import Ws.Model exposing (..)


type Mode
    = Edit
    | Add


type alias LeftPanel =
    { route : Route
    , remotesCount : Int
    , remotesDict : Dict String String
    , addRemotesDict : Dict Int ( String, String )
    , branches : List String
    , frequency : String
    , shortName : String
    , messages : Dict String ( Time, WebSocketMessage )
    , currentTime : Time
    , repoToRemove : String
    , language : Language
    , region : Maybe Region
    }


defaultLeftPanel : LeftPanel
defaultLeftPanel =
    { route = Home
    , remotesCount = 0
    , remotesDict = Dict.empty
    , addRemotesDict = Dict.empty
    , branches = []
    , frequency = ""
    , shortName = ""
    , messages = Dict.empty
    , currentTime = 0
    , repoToRemove = ""
    , language = English
    , region = Just UnitedStates
    }
