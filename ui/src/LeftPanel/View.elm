module LeftPanel.View exposing (..)

import Date exposing (fromTime)
import Date.Extra.Duration exposing (DeltaRecord, diff)
import Date.Format exposing (format)
import Dict exposing (Dict, keys)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode exposing (map)
import LeftPanel.Messages exposing (..)
import LeftPanel.Model exposing (LeftPanel, Mode(..))
import LeftPanel.RepoFormView exposing (originFormGroup, repoForm)
import Routing.Router exposing (..)
import Time exposing (Time)
import Ws.Model exposing (WebSocketMessage)


panelContent : String -> List (Html a) -> Html a
panelContent heading body =
    div [ class "panel panel-default" ]
        [ div [ class "panel-heading" ] [ text heading ]
        , div [ class "panel-body" ] body
        ]


helpButton : String -> Html Msg
helpButton id =
    div
        [ class "col-lg-2 col-md-2 col-sm-2 col-xs-3"
        , attribute "data-toggle" "tooltip"
        , title "Help"
        ]
        [ button
            [ class "btn btn-default"
            , type_ "button"
            , attribute "data-toggle" "collapse"
            , attribute "data-target" ("#" ++ id)
            , attribute "aria-expanded" "false"
            , attribute "aria-controls" id
            ]
            [ span [ class "glyphicon glyphicon-question-sign" ] [] ]
        ]


helpTextWell : String -> String -> Html Msg
helpTextWell idStr helpText =
    div [ class "row" ]
        [ div [ class "col-lg-8 col-lg-offset-2 col-md-7 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-6 col-xs-offset-3" ]
            [ div [ class "collapse", id (idStr ++ "-help-well") ]
                [ div [ class "well well-sm" ] [ text helpText ]
                ]
            ]
        ]


addRepoFormGroupTextArea : Int -> String -> String -> String -> (String -> Msg) -> Html Msg
addRepoFormGroupTextArea ti idStr labelStr placeholderStr msg =
    div [ class "form-group" ]
        [ label [ for idStr, class "col-lg-2 col-md-3 col-sm-2 col-xs-3 control-label" ] [ text labelStr ]
        , div [ class "col-lg-8 col-md-7 col-sm-8 col-xs-6" ]
            [ textarea [ tabindex ti, id idStr, class "form-control ta-sized", placeholder placeholderStr, onInput msg ] [] ]
        , helpButton (idStr ++ "-help-well")
        ]


addRepoFormGroupText : Int -> String -> String -> String -> (String -> Msg) -> Html Msg
addRepoFormGroupText ti idStr labelStr placeholderStr msg =
    div [ class "form-group" ]
        [ label [ for idStr, class "col-lg-2 col-md-3 col-sm-2 col-xs-3 control-label" ] [ text labelStr ]
        , div [ class "col-lg-8 col-md-7 col-sm-8 col-xs-6" ]
            [ input [ tabindex ti, type_ "text", class "form-control", id idStr, placeholder placeholderStr, onInput msg ] [] ]
        , helpButton (idStr ++ "-help-well")
        ]


onBlurb : (String -> msg) -> Attribute msg
onBlurb stuff =
    on "blur" (Json.Decode.map stuff targetValue)


additionalRemote : Int -> Html Msg
additionalRemote idx =
    let
        idStr =
            "remote-" ++ (toString idx)

        urlStr =
            "remote-" ++ (toString idx) ++ "-url"
    in
        div [ class "form-group" ]
            [ label [ for idStr, class "col-lg-2 col-md-3 col-sm-2 col-xs-3 control-label" ] [ text "Remote" ]
            , div [ class "col-lg-8 col-md-7 col-sm-8 col-xs-6" ]
                [ div [ class "row" ]
                    [ div [ class "col-lg-4 col-md-4 col-sm-4 col-xs-4" ]
                        [ input
                            [ tabindex (idx + 2)
                            , type_ "text"
                            , class "form-control"
                            , id idStr
                            , placeholder "github"
                            , onInput (ForSelf << SetAdditionalRepoKey idx)
                            , onBlurb (ForSelf << NewRemoteRow)
                            ]
                            []
                        ]
                    , div [ class "col-lg-8 col-md-8 col-sm-8 col-xs-8" ]
                        [ input
                            [ tabindex (idx + 3)
                            , type_ "text"
                            , class "form-control"
                            , id urlStr
                            , placeholder "git@github.com:CraZySacX/ellmak.git"
                            , onInput (ForSelf << SetAdditionalRepoValue idx)
                            ]
                            []
                        ]
                    ]
                ]
            ]


refsExample : String
refsExample =
    "origin/master\nrefs/heads/github/master\nrefs/heads/github/cool-feature"


formatTime : Time -> String
formatTime time =
    format "%Y-%m-%dT%H:%M:%S" (Date.fromTime time)


zeroPad : Int -> String
zeroPad val =
    if val >= 0 && val <= 9 then
        "0" ++ (toString val)
    else
        (toString val)


formatDuration : LeftPanel -> Time -> String
formatDuration model time =
    let
        dr =
            diff (Date.fromTime model.currentTime) (Date.fromTime time)
    in
        (zeroPad dr.hour) ++ ":" ++ (zeroPad dr.minute) ++ ":" ++ (zeroPad dr.second)


formatRef : Ws.Model.Ref -> String
formatRef ref =
    ref.ref ++ ": commit at " ++ ref.lastUpdated


formatRepoMessage : Ws.Model.Repo -> Time -> String
formatRepoMessage rm time =
    let
        refsAsString =
            String.concat <| List.intersperse "\n" <| List.map formatRef rm.refs
    in
        refsAsString ++ "\n\n" ++ rm.version ++ "\n" ++ rm.uname


displayMessage : LeftPanel -> ( String, ( Time, WebSocketMessage ) ) -> Html Msg
displayMessage model msg =
    let
        ( uuid, tsm ) =
            msg

        ( clazz, info, timer, detail ) =
            case tsm of
                ( time, Ws.Model.ErrorMessage err ) ->
                    ( "list-group-item-danger"
                    , p [ class "pad-top-8" ] [ text <| (formatTime time) ++ " " ++ err.name ++ ": " ++ err.message ]
                    , p [ class "pad-top-8" ] [ text (formatDuration model time) ]
                    , pre [] [ text err.stack ]
                    )

                ( time, Ws.Model.RepoMessage rm ) ->
                    ( "list-group-item-info"
                    , p [ class "pad-top-8" ]
                        [ text <|
                            (formatTime time)
                                ++ " "
                                ++ rm.subject
                        ]
                    , p [ class "pad-top-8" ] [ text (formatDuration model time) ]
                    , pre [] [ text <| formatRepoMessage rm time ]
                    )

                ( time, wsm ) ->
                    ( "list-group-item-warning"
                    , p [ class "pad-top-8" ] [ text <| (formatTime time) ++ " " ++ (toString wsm) ]
                    , p [ class "pad-top-8" ] [ text (formatDuration model time) ]
                    , pre [] [ text "Unknown Message Type" ]
                    )
    in
        li [ class ("list-group-item " ++ clazz) ]
            [ div [ class "row" ]
                [ div [ class "col-xs-9 col-sm-9 col-md-9 col-lg-9" ] [ info ]
                , div [ class "col-xs-1 col-sm-1 col-md-1 col-lg-1" ] [ timer ]
                , div [ class "col-xs-2 col-sm-2 col-md-2 col-lg-2" ]
                    [ div [ class "btn-group pull-right" ]
                        [ button
                            [ class "btn btn-default error-toggle collapsed"
                            , attribute "data-toggle" "collapse"
                            , attribute "data-target" ("#" ++ uuid)
                            ]
                            [ span
                                [ class "if-collapsed glyphicon glyphicon-chevron-down"
                                , attribute "data-toggle" "tooltip"
                                , title "Expand Details"
                                ]
                                []
                            , span
                                [ class "if-not-collapsed glyphicon glyphicon-chevron-up"
                                , attribute "data-toggle" "tooltip"
                                , title "Colapse Details"
                                ]
                                []
                            ]
                        , button
                            [ class "btn btn-default"
                            , attribute "data-toggle" "tooltip"
                            , title "Remove Message"
                            , onClick <| ForSelf <| RemoveMessage uuid
                            ]
                            [ span [ class "glyphicon glyphicon-remove" ] []
                            ]
                        ]
                    ]
                ]
            , div [ id uuid, class "collapse row pad-top-8" ]
                [ div [ class "col-xs-12 col-sm-12 col-md-12 col-lg-12" ] [ detail ]
                ]
            ]


messageSorter : ( String, ( Time, WebSocketMessage ) ) -> ( String, ( Time, WebSocketMessage ) ) -> Order
messageSorter a b =
    let
        ( _, ( atime, _ ) ) =
            a

        ( _, ( btime, _ ) ) =
            b
    in
        compare btime atime


sortMessages : Dict String ( Time, WebSocketMessage ) -> List ( String, ( Time, WebSocketMessage ) )
sortMessages messages =
    List.sortWith messageSorter <| Dict.toList messages


repoPanel : LeftPanel -> Mode -> Html Msg
repoPanel model mode =
    let
        ( headerText, content ) =
            case mode of
                Edit ->
                    ( "Edit Repository", repoForm model mode )

                Add ->
                    ( "Add Repository", div [] [] )
    in
        panelContent headerText [ content ]


view : LeftPanel -> Html Msg
view model =
    case model.route of
        Home ->
            let
                messagesLength =
                    Dict.size model.messages

                content =
                    case (compare messagesLength 0) of
                        GT ->
                            ul [ class "list-group" ] <| List.map (displayMessage model) <| sortMessages model.messages

                        LT ->
                            ul [ class "list-group" ]
                                [ li [ class "list-group-item list-group-item-success" ]
                                    [ p [ class "pad-top-8" ] [ text "No messages" ]
                                    ]
                                ]

                        EQ ->
                            ul [ class "list-group" ]
                                [ li [ class "list-group-item list-group-item-success" ]
                                    [ p [ class "pad-top-8" ] [ text "No messages" ]
                                    ]
                                ]
            in
                panelContent "Messages" [ content ]

        EditRepo ->
            repoPanel model Edit

        Routing.Router.RemoveRepo ->
            panelContent "Remove Repository"
                [ div [ class "row" ]
                    [ div [ class "col-xs-12 col-sm-12 col-md-12 col-lg-12" ]
                        [ if String.isEmpty model.repoToRemove then
                            p [] [ text "There is no repository selected to remove" ]
                          else
                            p []
                                [ strong [] [ text <| "Are you sure you wish to remove '" ++ model.repoToRemove ++ "'?" ]
                                ]
                        ]
                    , div [ class "col-xs-2 col-xs-offset-10 col-sm-2 col-sm-offset-10 col-md-12 col-md-offset-10 col-lg-12 col-lg-offset-10" ]
                        [ button
                            [ type_ "button"
                            , class "btn btn-default"
                            , onClick (ForParent (LeftPanel.Messages.RemoveRepo model.repoToRemove))
                            ]
                            [ text "Yes"
                            ]
                        , button [ type_ "button", class "btn btn-primary", onClick (ForSelf ToHome) ]
                            [ text "No"
                            ]
                        ]
                    ]
                ]

        AddRepo ->
            panelContent "Add Repository Monitor"
                [ Html.form [ class "form-horizontal", onSubmit (ForSelf Eat) ]
                    [ div [ class "row" ]
                        [ div [ class "col-lg-8 col-lg-offset-2 col-md-7 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-6 col-xs-offset-3" ]
                            [ div [ class "row pad-bottom-5" ]
                                [ div [ class "col-lg-4 col-md-4 col-sm-4 col-xs-4" ]
                                    [ div [ class "text-center" ] [ strong [] [ text "Remote Name" ] ]
                                    ]
                                , div [ class "col-lg-8 col-md-8 col-sm-8 col-xs-8" ]
                                    [ div [ class "text-center" ] [ strong [] [ text "Remote URL" ] ]
                                    ]
                                ]
                            ]
                        ]
                    , originFormGroup model Add
                    , div [] (List.map additionalRemote (List.range 0 model.remotesCount))
                    , addRepoFormGroupTextArea ((2 * model.remotesCount) + 4) "repo-refs" "Refs To Monitor" refsExample (ForSelf << SetBranches)
                    , helpTextWell "repo-refs" "Branches To Monitor Help Text"
                    , addRepoFormGroupText ((2 * model.remotesCount) + 5) "frequency" "Frequency" "15m" (ForSelf << SetFrequency)
                    , helpTextWell "frequency" "Frequency Help Text"
                    , addRepoFormGroupText ((2 * model.remotesCount) + 6) "short-name" "Short Name" "ellmak" (ForSelf << SetShortName)
                    , helpTextWell "short-name" "Short Name Help Text"
                    ]
                , div [ class "row" ]
                    [ div [ class "col-lg-4 col-lg-offset-6 col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-6 col-xs-offset-3" ]
                        [ div [ class "btn-group pull-right" ]
                            [ button [ class "btn btn-default", tabindex ((2 * model.remotesCount) + 7), onClick (ForSelf ClickAddRepo) ] [ text "Add" ]
                            , button [ class "btn btn-default", tabindex ((2 * model.remotesCount) + 8), onClick (ForSelf ToHome) ] [ text "Cancel" ]
                            ]
                        ]
                    ]
                ]

        NotFound ->
            panelContent "Not Found"
                [ p [] [ text "The resource you have requested cannot be found" ]
                ]
