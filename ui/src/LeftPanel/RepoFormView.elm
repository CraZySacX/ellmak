module LeftPanel.RepoFormView exposing (..)

import Html exposing (..)
import Html.Attributes exposing (readonly, tabindex)
import Html.AttributesExt exposing (..)
import Html.Model exposing (..)
import Html.Events exposing (on, onInput, onSubmit, targetValue)
import I18n.Model exposing (I18nId(..))
import I18n.Utils exposing (translate)
import Json.Decode exposing (map)
import LeftPanel.Messages exposing (..)
import LeftPanel.Model exposing (LeftPanel, Mode(..))


type alias TextInputConfig =
    { model : LeftPanel
    , mode : Mode
    , inputId : Id
    , labelId : I18nId
    , helpWellId : Id
    , placeholderId : Placeholder
    , tabIndex : Int
    , handler : String -> Msg
    }


repoForm : LeftPanel -> Mode -> Html Msg
repoForm model mode =
    Html.form [ class <| Bootstrap FormHorizontal, onSubmit (ForSelf Eat) ]
        [ remoteHeaderRow model
        , originFormGroup model mode
        , div [] <| List.map additionalRemote (List.range 0 model.remotesCount)
        , refsFormGroup model mode ((2 * model.remotesCount) + 4)
        , helpTextWell RepoRefsHelpWell "References To Monitor Help Text"
        , textInputFormGroup
            { model = model
            , mode = mode
            , inputId = FrequencyInput
            , labelId = Frequency
            , helpWellId = FrequencyHelpWell
            , placeholderId = FreqText
            , tabIndex = ((2 * model.remotesCount) + 5)
            , handler = (ForSelf << SetFrequency)
            }
        , helpTextWell FrequencyHelpWell "Frequency Help Text"
        , textInputFormGroup
            { model = model
            , mode = mode
            , inputId = ShortNameInput
            , labelId = ShortName
            , helpWellId = ShortNameHelpWell
            , placeholderId = ShortNameText
            , tabIndex = ((2 * model.remotesCount) + 6)
            , handler = (ForSelf << SetShortName)
            }
        , helpTextWell ShortNameHelpWell "ShortName Help Text"
        ]


remoteHeaderRow : LeftPanel -> Html Msg
remoteHeaderRow model =
    let
        { language, region } =
            model

        remoteNameText =
            translate language region RemoteName

        remoteURLText =
            translate language region I18n.Model.RemoteURL
    in
        div [ class <| Bootstrap Row ]
            [ div [ classList [ cl8, clo2, cm7, cmo3, cs8, cso2, cxs6, cxso3 ] ]
                [ div [ classList [ Bootstrap Row, Custom PadBottom5 ] ]
                    [ div [ classList [ cl4, cm4, cs4, cxs4 ] ]
                        [ div [ class <| Bootstrap TextCenter ] [ strong [] [ text remoteNameText ] ]
                        ]
                    , div [ classList [ cl8, cm8, cs8, cxs8 ] ]
                        [ div [ class <| Bootstrap TextCenter ] [ strong [] [ text remoteURLText ] ]
                        ]
                    ]
                ]
            ]


originFormGroup : LeftPanel -> Mode -> Html Msg
originFormGroup model mode =
    let
        originText =
            translate model.language model.region I18n.Model.Origin

        originAttributes =
            [ type_ Text
            , class <| Bootstrap FormControl
            , id OriginName
            , readonly True
            , placeholder Html.Model.Origin
            ]

        originUrlBaseAttributes =
            [ tabindex 1
            , type_ Text
            , class <| Bootstrap FormControl
            , id OriginUrl
            , onInput (ForSelf << SetOriginRemote)
            ]

        originUrlAttributes =
            case mode of
                Add ->
                    (placeholder Html.Model.RemoteURL) :: originUrlBaseAttributes

                Edit ->
                    originUrlBaseAttributes
    in
        div [ class <| Bootstrap FormGroup ]
            [ label [ for OriginName, classList [ cl2, cm3, cs2, cxs3, Custom ControlLabel ] ]
                [ text originText ]
            , div [ classList [ cl8, cm7, cs8, cxs6 ] ]
                [ div [ class <| Bootstrap Row ]
                    [ div [ classList [ cl4, cm4, cs4, cxs4 ] ] [ input originAttributes [] ]
                    , div [ classList [ cl8, cm8, cs8, cxs8 ] ] [ input originUrlAttributes [] ]
                    ]
                ]
            ]


refsFormGroup : LeftPanel -> Mode -> Int -> Html Msg
refsFormGroup model mode tabIndex =
    let
        refsToMonitorText =
            translate model.language model.region RefsToMonitor
    in
        div [ class <| Bootstrap FormGroup ]
            [ label [ for RepoRefs, classList [ cl2, cm3, cs2, cxs3, Custom ControlLabel ] ] [ text refsToMonitorText ]
            , div [ classList [ cl8, cm7, cs8, cxs6 ] ] [ refsTextArea mode tabIndex ]
            , helpButton model RepoRefsHelpWell
            ]


refsTextArea : Mode -> Int -> Html Msg
refsTextArea mode tabIndex =
    let
        baseAttributes =
            [ tabindex tabIndex
            , id RepoRefs
            , classList [ Bootstrap FormControl, Custom SizedTextArea ]
            , onInput (ForSelf << SetBranches)
            ]

        attributes =
            case mode of
                Add ->
                    (placeholder RefsTextArea) :: baseAttributes

                Edit ->
                    baseAttributes
    in
        textarea attributes []


helpButton : LeftPanel -> Id -> Html Msg
helpButton model helpWellId =
    let
        { language, region } =
            model
    in
        div
            [ classList [ cl2, cm2, cs2, cxs3 ]
            , attribute <| Data <| Toggle Tooltip
            , title language region Help
            ]
            [ button
                [ classList [ Bootstrap Btn, Bootstrap BtnDefault ]
                , type_ Button
                , attribute <| Data <| Toggle Coll
                , attribute <| Data <| Target helpWellId
                , attribute <| Aria <| Expanded False
                , attribute <| Aria <| Controls helpWellId
                ]
                [ span [ classList [ Bootstrap Glyphicon, Bootstrap GlyphiconQuestionSign ] ] [] ]
            ]


helpTextWell : Id -> String -> Html Msg
helpTextWell wellId helpText =
    div [ class <| Bootstrap Row ]
        [ div [ classList [ cl8, clo2, cm7, cmo3, cs8, cso2, cxs6, cxso3 ] ]
            [ div [ class <| Bootstrap Collapse, id wellId ]
                [ div [ classList [ Bootstrap Well, Bootstrap WellSm ] ] [ text helpText ]
                ]
            ]
        ]


textInputFormGroup : TextInputConfig -> Html Msg
textInputFormGroup { model, mode, inputId, labelId, placeholderId, helpWellId, tabIndex, handler } =
    let
        labelText =
            translate model.language model.region labelId

        baseAttributes =
            [ tabindex tabIndex
            , type_ Text
            , class <| Bootstrap FormControl
            , id inputId
            , onInput handler
            ]

        attributes =
            case mode of
                Add ->
                    (placeholder placeholderId) :: baseAttributes

                Edit ->
                    baseAttributes
    in
        div [ class <| Bootstrap FormGroup ]
            [ label [ for inputId, classList [ cl2, cm3, cs2, cxs3, Custom ControlLabel ] ] [ text labelText ]
            , div [ classList [ cl8, cm7, cs8, cxs6 ] ] [ input attributes [] ]
            , helpButton model helpWellId
            ]


additionalRemote : Int -> Html Msg
additionalRemote idx =
    let
        inputId =
            RemoteInput idx

        urlInputId =
            RemoteUrlInput idx
    in
        div [ class <| Bootstrap FormGroup ]
            [ label [ for inputId, classList [ cl2, cm3, cs2, cxs3, Custom ControlLabel ] ] [ text "Remote" ]
            , div [ classList [ cl8, cm7, cs8, cxs6 ] ]
                [ div [ class <| Bootstrap Row ]
                    [ div [ classList [ cl4, cm4, cs4, cxs4 ] ]
                        [ input
                            [ tabindex (idx + 2)
                            , type_ Text
                            , class <| Bootstrap FormControl
                            , id inputId
                            , placeholder RemoteId
                            , onInput (ForSelf << SetAdditionalRepoKey idx)
                            , onBlurb (ForSelf << NewRemoteRow)
                            ]
                            []
                        ]
                    , div [ classList [ cl8, cm8, cs8, cxs8 ] ]
                        [ input
                            [ tabindex (idx + 3)
                            , type_ Text
                            , class <| Bootstrap FormControl
                            , id urlInputId
                            , placeholder RemoteGithubUrl
                            , onInput (ForSelf << SetAdditionalRepoValue idx)
                            ]
                            []
                        ]
                    ]
                ]
            ]


onBlurb : (String -> msg) -> Attribute msg
onBlurb stuff =
    on "blur" (Json.Decode.map stuff targetValue)
