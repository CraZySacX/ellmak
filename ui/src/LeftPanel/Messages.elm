module LeftPanel.Messages exposing (..)

import I18n.Model exposing (Language, Region)
import LeftPanel.Model exposing (LeftPanel)
import Time exposing (Time)


type InternalMsg
    = -- UI Messages
      Eat
    | NewRemoteRow String
    | SetLanguageRegion ( Language, Maybe Region )
      -- Navigation Messages
    | ToHome
      -- Input Messages
    | ClickAddRepo
    | RemoveMessage String
    | SetOriginRemote String
    | SetAdditionalRepoKey Int String
    | SetAdditionalRepoValue Int String
    | SetBranches String
    | SetFrequency String
    | SetShortName String
    | Tick Time


type ExternalMsg
    = PostRepo LeftPanel
    | RemoveRepo String
    | SendWsMessage String


type Msg
    = ForSelf InternalMsg
    | ForParent ExternalMsg


type alias TranslationDictionary msg =
    { onInternalMessage : InternalMsg -> msg
    , onPostRepo : LeftPanel -> msg
    , onRemoveRepo : String -> msg
    , onSendWsMessage : String -> msg
    }


type alias Translator parentMsg =
    Msg -> parentMsg


translator : TranslationDictionary parentMsg -> Translator parentMsg
translator { onInternalMessage, onPostRepo, onRemoveRepo, onSendWsMessage } msg =
    case msg of
        ForSelf internal ->
            onInternalMessage internal

        ForParent (PostRepo model) ->
            onPostRepo model

        ForParent (RemoveRepo repo) ->
            onRemoveRepo repo

        ForParent (SendWsMessage message) ->
            onSendWsMessage message
