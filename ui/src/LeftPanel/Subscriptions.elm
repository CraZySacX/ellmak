module LeftPanel.Subscriptions exposing (..)

import LeftPanel.Messages exposing (InternalMsg(Tick))
import LeftPanel.Model exposing (LeftPanel)
import Time exposing (second, Time)


subscriptions : LeftPanel -> Sub InternalMsg
subscriptions model =
    Time.every second Tick
