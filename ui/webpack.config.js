var webpack = require('webpack')
var cssnano = require('cssnano')
var path = require('path')
var _debug = require('debug')
var env = require('./env')
var CopyWebpackPlugin = require('copy-webpack-plugin')

const debug = _debug('app:webpack:config')
const {__DEV__, __INT__, __STG__, __PROD__} = env.globals

debug('Webpack Configuration')
const rootPublic = path.resolve('./static')
const outputPath = path.resolve('./dist')
const modulePaths = path.resolve(__dirname, "./styles")

const config = {
  output: {
    path: outputPath,
    filename: 'index.js'
  },

  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.elm']
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: `css-loader?sourceMap&root=${rootPublic}&-minimize`
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: `css-loader?sourceMap&root=${rootPublic}&-minimize`
          },
          {
            loader: 'sass-loader?sourceMap',
            options: {
              includePaths: [path.resolve(__dirname, "./styles")]
            }
          }
        ]
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      },
      {
        test: /\.(png|jpg|svg|woff|woff2)?(\?v=\d+.\d+.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 8912
        }
      },
      {
        test: /\.(eot|ttf)$/,
        loader: 'file-loader'
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        loader: 'elm-hot-loader!elm-webpack-loader'
      },
      {
        test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
        loader: 'imports-loader?jQuery=jquery'
      },
    ],

    noParse: /\.elm$/
  }
};

config.plugins = []

config.plugins.push(new CopyWebpackPlugin([{from: 'static', ignore: ['black-brick-wall-texture.jpg']}]))

if (__DEV__) {
  debug('Enable plugins for live development (CopyWebpack, Define, NoEmitOnErrors).')
  config.output.publicPath="http://localhost:8080/"
  config.plugins.push(
    new webpack.DefinePlugin({
      __DEV__: __DEV__,
      __INT__: __INT__,
      __STG__: __STG__,
      __PROD__: __PROD__,
      BASE_URL: JSON.stringify("http://localhost:3000/api/v1"),
      WS_BASE_URL: JSON.stringify("ws://localhost:3000"),
      UI_VERSION: JSON.stringify("v" + require("./package.json").version),
      API_VERSION: JSON.stringify("v" + require("../api/package.json").version)
    }),
    new webpack.NoEmitOnErrorsPlugin()
  )

  config.entry = [ 'bootstrap-loader', './src/index.js' ]
}
if (__INT__ || __STG__ || __PROD__) {
  debug('Enable plugins for production (CopyWebpack, Define, OccurenceOrder, & UglifyJS).')
  config.output.publicPath="https://ellmak.io/"
  config.plugins.push(
    new webpack.DefinePlugin({
      __DEV__: __DEV__,
      __INT__: __INT__,
      __STG__: __STG__,
      __PROD__: __PROD__,
      BASE_URL: JSON.stringify("api/v1"),
      WS_BASE_URL: JSON.stringify("wss://ellmak.io/api/ws"),
      UI_VERSION: JSON.stringify("v" + require("./package.json").version),
      API_VERSION: JSON.stringify("v" + require("../api/package.json").version)
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        unused: true,
        dead_code: true,
        warnings: false
      }
    })
  )

  config.entry = [ 'bootstrap-loader', './src/index.js' ]
}

module.exports = config
